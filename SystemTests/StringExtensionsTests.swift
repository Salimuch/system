//
//  StringExtensionsTests.swift
//  System
//
//  Created by Artem Salimyanov on 02.02.17.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import XCTest
@testable import System

class StringExtensionsTests: XCTestCase {
    
    func testCleanPhoneFormat() {
        let phoneNumber1 = "+7 (919) 111-22-33"
        let cleanedPhoneNumber = phoneNumber1.clearPhoneFormat()
        XCTAssertEqual(cleanedPhoneNumber, "9191112233")
    }
    
    func testClearCardFormat() {
        let cardNumber = "1111-2222 / 3333  : 4444"
        let cleanedCardNumber = cardNumber.clearCardFormat()
        XCTAssertEqual(cleanedCardNumber, "1111222233334444")
    }
    
}
