//
//  PlistManagerTests.swift
//  System
//
//  Created by Artem Salimyanov on 02.02.17.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import XCTest
@testable import System

class PlistManagerTests: XCTestCase {
    
    func testSave() {
        PlistManager.save(value: "new test record" as AnyObject, key: "test")
        if let newObject = PlistManager.property("test") as? String {
            XCTAssertEqual(newObject, "new test record")
        } else {
            XCTAssert(false)
        }
    }
    
    func testRemove() {
        let asyncExpectation = expectation(description: "Dummy expectation")
        
        PlistManager.remove("test")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            let nilObject = PlistManager.property("test")
            XCTAssert(nilObject == nil)
            asyncExpectation.fulfill()
        }
        self.waitForExpectations(timeout: 20, handler: nil)
    }
    
}
