//
//  DeviceInfo.swift
//  System
//
//  Created by Artem Salimyanov on 27.03.17.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import Foundation

public struct DeviceInfo {
    public let osName: String
    public let osVersion: String
    public let deviceModel: String
    public let appVersion: String
    public var deviceIdForVendor: String?
    
    public init() {
        let device = UIDevice.current
        
        self.osName = device.systemName
        self.osVersion = device.systemVersion
        self.deviceModel = device.deviceType.rawValue
        
        if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String,
            let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
        {
            self.appVersion = "\(version)(\(build))"
        } else {
            self.appVersion = ""
        }
    }
}
