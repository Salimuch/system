//
//  PlistManager.swift
//  System
//
//  Created by Артем Шляхтин on 07.10.14.
//  Copyright (c) 2014 Май-Tech. All rights reserved.
//

import UIKit

public class PlistManager {
    
    /**
     Возвращает объект по указанному ключу.
     
     - parameter key: Ключ.
     - returns: В случае успеха возвращает найденое свойство. В другом случае возвращает nil.
    */
    public class func property(_ key: String) -> AnyObject? {
        let userDefaultProperty = UserDefaults.standard
        return userDefaultProperty.object(forKey: key) as AnyObject?
    }
    
    /**
     Сохраняет объект по указанному ключу.
     
     - parameter value: Объект.
     - parameter key: Ключ.
    */
    public class func save(value: AnyObject, key: String) {
        let userDefaultProperty = UserDefaults.standard
        userDefaultProperty.set(value, forKey: key)
        userDefaultProperty.synchronize()
    }
    
    /**
     Удаляет объект по указанному ключу.
     
     - parameter key: Ключ.
    */
    public class func remove(_ key: String) {
        let userDefaultProperty = UserDefaults.standard
        userDefaultProperty.removeObject(forKey: key)
        userDefaultProperty.synchronize()
    }
}
