//
//  DeviceInterface.swift
//  System
//
//  Created by Артем Шляхтин on 31.08.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

public struct ScreenSize
{
    public static let Center       = CGPoint(x: Width/2.0, y: Height/2.0)
    public static let Width        = UIScreen.main.bounds.size.width
    public static let Height       = UIScreen.main.bounds.size.height
    public static let MaxLength    = max(ScreenSize.Width, ScreenSize.Height)
    public static let MinLength    = min(ScreenSize.Width, ScreenSize.Height)
}
