//
//  TMPLOperation.swift
//  System
//
//  Created by Артем Шляхтин on 21.10.15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

open class OperationTemplate: Operation {
    
    private var finishInParent: Bool
    private var _executing = false
    private var _finished = false
    private var _ready = true
    
    
    // MARK: - Lifecycle
    
    /**
     Инициализация новой операции
     
     - parameter completionBlock: Блок запускается после того, как операция завершит свою работу.
     - parameter finishInParent: Завершается ли операция в родительском классе или нет.
    */
    public init(completionBlock: (() -> Void)? = nil, finishInParent: Bool = true) {
        self.finishInParent = finishInParent
        super.init()
        self.completionBlock = completionBlock
        self.name = "custom"
    }
    
    open override func start() {
        if isCancelled {
            finish()
            return
        }
        
        isExecuting = true
        main()
    }
    
    open override func main() {
        if isCancelled == true && _finished != false {
            finish()
            return
        }
        
        if finishInParent { finish() }
    }
    
    open override func cancel() {
        super.cancel()
        finish()
    }
    
    /**
     Функция переводит операцию в статус Завершена.
    */
    public func finish() {
        isExecuting = false
        isFinished = true
    }
    
    
    // MARK: - Properties
    
    /**
     Свойство указывает готова ли операция к запуску.
    */
    open override var isReady : Bool {
        get { return _ready && super.isReady }
        set {
            willChangeValue(forKey: "isReady")
            _ready = newValue
            didChangeValue(forKey: "isReady")
        }
    }
    
    /**
     Свойство указывает запущена ли операция.
    */
    open override var isExecuting : Bool {
        get { return _executing }
        set {
            willChangeValue(forKey: "isExecuting")
            _executing = newValue
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    /**
     Свойство указывает завершена ли работа операции.
    */
    open override var isFinished : Bool {
        get { return _finished }
        set {
            willChangeValue(forKey: "isFinished")
            _finished = newValue
            didChangeValue(forKey: "isFinished")
        }
    }
    
}
