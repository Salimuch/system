//
//  UIViewController+Additional.swift
//  System
//
//  Created by Артём Шляхтин on 09/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

extension UIViewController {
    
    public var topViewController: UIViewController? {
        get {
            var topController = UIApplication.shared.windows.first?.rootViewController
            while topController?.presentedViewController != nil {
                topController = topController?.presentedViewController
            }
            return topController
        }
    }
    
}
