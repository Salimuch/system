//
//  NSDate+NSCalendar.swift
//  System
//
//  Created by Артем Шляхтин on 21/09/15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

extension Date {
    
    // MARK: - Extract Date
    
    /**
     Функция возвращает день из текущей даты.
    */
    public func day() -> Int {
        return self.components().day!
    }
    
    /**
     Функция возвращает месяц из текущей даты.
    */
    public func month() -> Int {
        return self.components().month!
    }
    
    /**
     Функция возвращает год из текущей даты.
    */
    public func year() -> Int {
        return self.components().year!
    }
    
    /**
     Функция возвращает часы из текущей даты.
    */
    public func hours() -> Int {
        return self.components().hour!
    }
    
    /**
     Функция возвращает минуты из текущей даты.
    */
    public func minutes() -> Int {
        return self.components().minute!
    }
    
    /**
     Функция возвращает день с обнуленной датой.
    */
    public func dateAtBeginningOfDay() -> Date? {
        let timeZone = TimeZone.current
        var calendar = Calendar.current
        calendar.timeZone = timeZone
        
        var components = calendar.dateComponents([.day, .month, .year], from: self)
        components.hour = 0
        components.minute = 0
        components.second = 0
        
        let date = calendar.date(from: components)
        return date
    }
    
    fileprivate func components() -> DateComponents {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day, .month, .year, .hour, .minute], from: self)
        return components
    }
    
}

// MARK: - Add Time

extension Date {

    /**
     Функция прибавляет минуты к текущей дате.
     
     - parameter minutes: Количество минут.
     - returns: Возвращает новую дату с учетом добавленных минут.
    */
    public func dateByAddingMinutes(_ minutes: TimeInterval) -> Date {
        return self.addingTimeInterval(minutes*60)
    }
    
    /**
     Функция прибавляет часы к текущей дате.
     
     - parameter hours: Количество часов.
     - returns: Возвращает новую дату с учетом добавленных часов.
    */
    public func dateByAddingHours(_ hours: TimeInterval) -> Date {
        return self.addingTimeInterval(hours*60*60)
    }
    
    /**
     Функция прибавляет дни к текущей дате.
     
     - parameter days: Количество дней.
     - returns: Возвращает новую дату с учетом добавленных дней.
    */
    public func dateByAddingDays(_ days: TimeInterval) -> Date {
        return self.addingTimeInterval(days*24*60*60)
    }
    
    // MARK: - In Words
    
    /**
     Функция преобразует дату в формат строки Месяц Год. Где месяц указан прописью.
     
     - returns: Возвращает строку с датой
    */
    public func inWords() -> String {
        var month: String
        
        switch self.month() {
            case 1: month = "Январь"
            case 2: month = "Февраль"
            case 3: month = "Март"
            case 4: month = "Апрель"
            case 5: month = "Май"
            case 6: month = "Июнь"
            case 7: month = "Июль"
            case 8: month = "Август"
            case 9: month = "Сентябрь"
            case 10: month = "Октябрь"
            case 11: month = "Ноябрь"
            case 12: month = "Декабрь"
        default:
            month = "\(self.month())"
        }
        return "\(month) \(self.year())"
    }
    
}
