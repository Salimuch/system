//
//  NSError+Additional.swift
//  System
//
//  Created by Артём Шляхтин on 21/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import Foundation

extension NSError {
    
    public class func create(_ code: Int, description: String) -> NSError {
        let name = bundleName()
        let dict = [NSLocalizedDescriptionKey: description]
        let error = NSError(domain: name, code: code, userInfo: dict)
        return error
    }
    
    class func bundleName() -> String {
        var identifier = SystemFramework.bundleName
        if let name = Bundle.main.bundleIdentifier {
            identifier = name
        }
        return identifier
    }
    
}
