//
//  NSFileManager+DocumentDirectory.swift
//  System
//
//  Created by Артем Шляхтин on 10/09/15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

extension FileManager {
    
    /**
     Функция возращает URL с сылкой на директорию Documents текущего приложения.
     
     - returns: В случае успеха возвращает URL на директорию, иначе возвращает nil.
    */
    public func applicationDocumentsDirectory() -> URL? {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        guard let directory = urls.first else { return nil }
        return directory
    }
    
}
