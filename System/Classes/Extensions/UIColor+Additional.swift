//
//  UIColor+System.swift
//  CardReader
//
//  Created by Артем Шляхтин on 04.09.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

extension UIColor {

    public class func keyboardColor() -> UIColor {
        return UIColor(red: 212.0/255.0, green: 216.0/255.0, blue: 222.0/255.0, alpha: 1.0)
    }
    
}
